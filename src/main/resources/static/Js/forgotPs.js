function checkPasswordMatch(fieldConfirmPassword) {
    if (fieldConfirmPassword.value != $("#password").val()) {
        fieldConfirmPassword.setCustomValidity("Passwords do not match!");
    } else {
        fieldConfirmPassword.setCustomValidity("");
    }
}


$(function () {
    $(window).on('scroll', function () {
        if ( $(window).scrollTop() > 10 ) {
            $('.navbar').addClass('active');
        } else {
            $('.navbar').removeClass('active');
        }
    });
});

 $('[data-toggle="switch"]').bootstrapSwitch();
 
 $(document).ready(function() {
  var slider = $("#slideshow");
  var slider_nav = $("#slideshow-nav");
  slider_nav.find("a[href=#slide1]").addclass("active");

  slider_nav.localscroll({
    target:'#slideshow',
    axis: 'x'
  });

  slider_nav.find("a").click(function(){
    slider_nav.find("a").removeclass("active");
    $(this).addclass("active");
  });
});

