package com.diploma.Webshop.Repositories;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.diploma.Webshop.Models.Product;

public interface PagingProductRepository extends PagingAndSortingRepository<Product, Long> {

}
