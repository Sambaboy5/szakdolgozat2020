package com.diploma.Webshop.Repositories;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.diploma.Webshop.Models.Category;
import com.diploma.Webshop.Models.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {
	@Query("SELECT u FROM Product u WHERE u.name = :name")
	public Product findByName(@Param("name") String name);

	@Query("SELECT u FROM Product u WHERE u.id = :id")
	public Product findByProductId(@Param("id") Long id);

	@Query("SELECT u FROM Category u WHERE u.id = :id")
	public Category findByCategoryId(@Param("id") Long id);

	@Query("SELECT u FROM Category u WHERE u.categoryName = :categoryName")
	public Product findProductByCategoryId(@Param("categoryName") String categoryName);

	@Transactional
	@Modifying
	@Query("delete from Product where id = ?1")
	void deleteProductByFirstName(@Param("id") Long id);

	@Transactional
	@Modifying
	@Query("update Product u set u.name = ?1, u.price = ?2, u.unitInStock = ?3, u.shortDescription = ?4 where u.id = ?5")
	void setProductInfoById(String name, Double price, Integer unitInStock, String shortDescription, Long id);

}
