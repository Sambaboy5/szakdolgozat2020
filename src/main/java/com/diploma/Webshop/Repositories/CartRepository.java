package com.diploma.Webshop.Repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.diploma.Webshop.Models.Cart;
import com.diploma.Webshop.Models.CartItem;

@Repository
public interface CartRepository extends JpaRepository<Cart, Long> {

	@Query("SELECT u FROM Cart u WHERE u.id = :id")
	public Cart findByCartId(@Param("id") Long id);

	@Transactional
	@Modifying
	@Query("delete from Cart where id = ?1")
	void deleteCart(@Param("id") Long id);

	@Transactional
	@Modifying
	@Query("delete from Cart where user = ?1")
	void deleteCartByUser(@Param("id") Long id);
	
	@Query("SELECT u FROM Cart u WHERE u.user = :id")
	List<Cart> findCartByUserId(@Param("id") Long id);

}
