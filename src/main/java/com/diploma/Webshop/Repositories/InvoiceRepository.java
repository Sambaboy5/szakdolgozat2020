package com.diploma.Webshop.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.diploma.Webshop.Models.Invoice;

public interface InvoiceRepository extends JpaRepository<Invoice, Long> {
	@Query("SELECT u FROM Invoice u WHERE u.id = :id")
	public Invoice findInvoiceById(@Param("id") Long id);
}
