package com.diploma.Webshop.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.diploma.Webshop.Models.BillingAddress;

public interface BillingRepository extends JpaRepository<BillingAddress, Long> {
	@Query("SELECT u FROM BillingAddress u WHERE u.id = :id")
	public BillingAddress findByBillingAddress(@Param("id") Long id);
}
