package com.diploma.Webshop.Repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.diploma.Webshop.Models.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	public User findByEmail(String email);

	@Query("SELECT u FROM User u WHERE u.id = :id")
	public User findByUserId(@Param("id") Long id);

	@Query("SELECT u FROM User u WHERE u.email = :email")
	public User findByUserByMail(@Param("email") String email);

	@Transactional
	@Modifying
	@Query("delete from User where id = ?1")
	void deleteUsersByFirstName(@Param("id") Long id);

	@Query("SELECT u FROM User u WHERE u.email = :email")
	List<User> findUserByEmail(@Param("email") String email);

	@Transactional
	@Modifying
	@Query("delete from Cart where user = ?1")
	void deleteCartByFirstName(@Param("id") Long id);

	@Query("SELECT u FROM User u WHERE u.verificationCode = ?1")
	public User findByVerificationCode(String code);

	public User findByResetPasswordToken(String token);

}
