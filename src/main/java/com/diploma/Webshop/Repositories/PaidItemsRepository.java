package com.diploma.Webshop.Repositories;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.diploma.Webshop.Models.PaidItems;


@Repository
public interface PaidItemsRepository extends JpaRepository<PaidItems, Long> {

	
	@Query("SELECT u FROM PaidItems u WHERE u.PaidItemsId = :PaidItemsId")
	public PaidItems findByPaidItemsId(@Param("PaidItemsId") Long id);
	
	@Transactional
	@Modifying
	@Query("delete from Contact where user = ?1")
	void deletePaidItemsByUserId(@Param("id") Long id);
}
