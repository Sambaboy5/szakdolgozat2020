package com.diploma.Webshop.Repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.diploma.Webshop.Models.Category;
import com.diploma.Webshop.Models.Product;

public interface CategoryRepository extends JpaRepository<Category, Long> {

	@Query("SELECT u FROM Category u WHERE u.categoryId = :categoryId")
	public Category findCategoryById(@Param("categoryId") Long id);

	@Query("SELECT u FROM Category u WHERE u.id = :id")
	public Category findByCategoryId(@Param("id") Long id);

	@Query("SELECT u FROM Category u WHERE u.categoryName = :categoryName")
	public Category findProductByCategoryName(@Param("categoryName") String categoryName);

	@Query("SELECT u FROM Product u WHERE u.category = :categoryName")
	List<Product> findProductByCategoryNames(@Param("categoryName") String categoryName);

}
