package com.diploma.Webshop.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.diploma.Webshop.Models.ShippingAddress;

public interface ShippingRepository extends JpaRepository<ShippingAddress, Long> {
	@Query("SELECT u FROM ShippingAddress u WHERE u.id = :id")
	public ShippingAddress findByShippingId(@Param("id") Long id);
}
