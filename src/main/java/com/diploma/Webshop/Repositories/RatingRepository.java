package com.diploma.Webshop.Repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.diploma.Webshop.Models.Product;
import com.diploma.Webshop.Models.Rating;

public interface RatingRepository extends JpaRepository<Rating, Long> {
	@Query("SELECT u FROM Rating u WHERE u.product = :product")
	List<Rating> findRatingByProduct(@Param("product") Product product);
	
	
	@Transactional
	@Modifying
	@Query("DELETE FROM Rating WHERE id = ?1")
	void deleteRatingByProduct(@Param("id") Long id);
}
