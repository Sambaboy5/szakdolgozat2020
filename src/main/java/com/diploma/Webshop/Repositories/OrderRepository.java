package com.diploma.Webshop.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.diploma.Webshop.Models.Order;

public interface OrderRepository extends JpaRepository<Order, Long> {
	@Query("SELECT u FROM Order u WHERE u.id = :id")
	public Order findByOrderId(@Param("id") Long id);
}
