package com.diploma.Webshop.Repositories;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.diploma.Webshop.Models.Contact;

public interface ContactRepository extends JpaRepository<Contact, Long> {
	@Query("SELECT u FROM Contact u WHERE u.id = :id")
	public Contact findContactById(@Param("id") Long id);

	@Query("SELECT u FROM Contact u WHERE u.email = :email")
	public Contact findContactByMail(@Param("email") String email);

	@Transactional
	@Modifying
	@Query("delete from Contact where id = ?1")
	void deleteContactById(@Param("id") Long id);

}
