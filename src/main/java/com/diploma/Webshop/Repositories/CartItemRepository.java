package com.diploma.Webshop.Repositories;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.diploma.Webshop.Models.CartItem;

@Repository
public interface CartItemRepository extends JpaRepository<CartItem, Long> {

	Optional<List<CartItem>> findByCartId(Long id);

	@Query("SELECT u FROM CartItem u WHERE u.cartItemId = :cartItemId")
	public CartItem findByCartItemId(@Param("cartItemId") Long id);

	@Query("SELECT u FROM CartItem u WHERE u.cart = :cart")
	public CartItem findByCartItemByCartId(@Param("cart") Long id);

	@Transactional
	@Modifying
	@Query("delete from CartItem where id = ?1")
	void deleteCartItem(@Param("id") Long id);
	
	@Query("delete FROM CartItem u WHERE u.cart = :id")
	List<CartItem> deleteCartItemsByCart(@Param("id") Long id);

	@Transactional
	@Modifying
	@Query("update CartItem u set u.totalPriceDouble = ?1, u.quantity = ?2 where u.id = ?3")
	void setCartItemInfoById(Double totalPriceDouble, Integer quantity, Long id);

	@Query("SELECT u FROM CartItem u WHERE u.product = :id")
	List<CartItem> findCartItemsByProductId(@Param("id") Long id);

	@Query("delete FROM CartItem u WHERE u.product = :id")
	List<CartItem> deleteCartItemsByProductId(@Param("id") Long id);

}
