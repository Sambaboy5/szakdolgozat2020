package com.diploma.Webshop.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.diploma.Webshop.Models.Contact;
import com.diploma.Webshop.Models.OldContact;

public interface OldContactRepository extends JpaRepository<OldContact, Long> {
	@Query("SELECT u FROM OldContact u WHERE u.id = :id")
	public OldContact findOldContactById(@Param("id") Long id);
}
