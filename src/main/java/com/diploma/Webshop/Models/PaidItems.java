package com.diploma.Webshop.Models;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "paid_Items")
public class PaidItems {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long PaidItemsId;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "cartId")
	@JsonIgnore
	private Cart cart;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "productId")
	private Product product;

	@SuppressWarnings("unused")
	private int totalPriceDouble;

	@SuppressWarnings("unused")
	private int quantity;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "userId")
	@JsonIgnore
	private User user;
	
	public Long getPaidItemsId() {
		return PaidItemsId;
	}

	public void setPaidItemsId(Long paidItemsId) {
		PaidItemsId = paidItemsId;
	}

	public Cart getCart() {
		return cart;
	}

	public void setCart(Cart cart) {
		this.cart = cart;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public int getTotalPriceDouble() {
		return totalPriceDouble;
	}

	public void setTotalPriceDouble(int totalPriceDouble) {
		this.totalPriceDouble = totalPriceDouble;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	
	
	

}
