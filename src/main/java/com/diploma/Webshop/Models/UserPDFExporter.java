package com.diploma.Webshop.Models;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.util.Base64Utils;

import com.lowagie.text.*;
import com.lowagie.text.pdf.*;

public class UserPDFExporter {

	private List<Order> orders;

	public UserPDFExporter(List<Order> orders) {
		this.orders = orders;
	}

	private void writeTableHeader(PdfPTable table) {
		PdfPCell cell = new PdfPCell();
		cell.setBackgroundColor(Color.BLUE);
		cell.setPadding(5);

		Font font = FontFactory.getFont(FontFactory.HELVETICA);
		font.setColor(Color.WHITE);

		cell.setPhrase(new Phrase("Termék", font));

		table.addCell(cell);

		cell.setPhrase(new Phrase("Darabszám", font));
		table.addCell(cell);

		cell.setPhrase(new Phrase("Ár", font));
		table.addCell(cell);
	}

	private void writeTableAddress(PdfPTable tableAddress) {
		PdfPCell cell = new PdfPCell();
		cell.setBackgroundColor(Color.BLUE);
		cell.setPadding(5);

		Font font = FontFactory.getFont(FontFactory.HELVETICA);
		font.setSize(10);
		font.setColor(Color.WHITE);

		cell.setPhrase(new Phrase("Név:", font));

		tableAddress.addCell(cell);

		cell.setPhrase(new Phrase("Cím:", font));
		tableAddress.addCell(cell);

		cell.setPhrase(new Phrase("Város:", font));
		tableAddress.addCell(cell);
		cell.setPhrase(new Phrase("Megye:", font));
		tableAddress.addCell(cell);
		cell.setPhrase(new Phrase("Ország:", font));
		tableAddress.addCell(cell);
	}

	private void writeAll(PdfPTable tableAll) {
		PdfPCell cell = new PdfPCell();
		cell.setBackgroundColor(Color.GREEN);
		cell.setPadding(5);

		Font font = FontFactory.getFont(FontFactory.HELVETICA);
		font.setColor(Color.WHITE);

		cell.setPhrase(new Phrase("Összesen", font));

		tableAll.addCell(cell);

	}

	private void writeTableData(PdfPTable table) {
		for (Order order : orders) {
			List<CartItem> cartItem = order.getCart().getUser().getCart().getCartItems();
			for (CartItem cartItems : cartItem) {
				table.addCell(cartItems.getProduct().getName());
				table.addCell(String.valueOf(cartItems.getQuantity()));
				table.addCell(String.valueOf(cartItems.getProduct().getPrice()));

			}

		}
	}

	private void writeAllData(PdfPTable tableAll) {
		for (Order order : orders) {
			tableAll.addCell(String.valueOf(order.getCart().getTotalPrice()));

		}
	}

	private void writeAllAddress(PdfPTable tableAddress) {
		Font font = FontFactory.getFont(FontFactory.HELVETICA);
		font.setSize(10);
		font.setColor(Color.WHITE);
		for (int i = 0; i < 1; i++) {
			for (Order order : orders) {

				tableAddress.addCell(order.getUser().getFirstName() + " " + order.getUser().getLastName());
				tableAddress.addCell(order.getCart().getUser().getShippingAddress().getAddress());
				tableAddress.addCell(order.getCart().getUser().getShippingAddress().getCity() + " "
						+ order.getCart().getUser().getBillingAddress().getZipcode().toString());
				tableAddress.addCell(order.getCart().getUser().getShippingAddress().getCounty());
				tableAddress.addCell(order.getCart().getUser().getShippingAddress().getCountry());
			}

		}
	}

	public byte[] export(HttpServletResponse response) throws DocumentException, IOException {
		Document document = new Document(PageSize.A4);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		PdfWriter.getInstance(document, baos);

		document.open();
		Font font = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
		font.setSize(30);
		font.setColor(Color.BLUE);

		Paragraph p = new Paragraph("Számla", font);
		p.setAlignment(Paragraph.ALIGN_CENTER);

		document.add(p);

		PdfPTable tableAddress = new PdfPTable(5);
		tableAddress.setWidthPercentage(80f);
		tableAddress.setWidths(new float[] { 2.0f, 1.5f, 3.0f, 1.5f, 1.5f });
		tableAddress.setSpacingBefore(50);

		tableAddress.setHorizontalAlignment(PdfPTable.ALIGN_LEFT);

		PdfPTable table = new PdfPTable(3);
		table.setWidthPercentage(100f);
		table.setWidths(new float[] { 1.5f, 1.5f, 3.0f });
		table.setSpacingBefore(50);

		PdfPTable tableAll = new PdfPTable(2);
		tableAll.setWidthPercentage(100f);
		tableAll.setWidths(new float[] { 1.5f, 1.5f });
		tableAll.setSpacingBefore(10);

		writeTableAddress(tableAddress);

		writeAllAddress(tableAddress);

		writeTableHeader(table);
		writeTableData(table);
		writeAll(tableAll);
		writeAllData(tableAll);
		document.add(tableAddress);
		document.add(table);
		document.add(tableAll);

		document.close();
		
		byte[] pdf = baos.toByteArray();
		
		return pdf;

	}
}
