package com.diploma.Webshop.Models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name = "products")
public class Product {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "Name", length = 255, nullable = false)
	private String name;

	@Column(name = "Short_Description", nullable = false, columnDefinition = "TEXT")
	private String shortDescription;

	@Column(name = "Price", nullable = false)
	private int price;

	@Column(name = "Unit_in_Stock", nullable = false)
	private int unitInStock;

	@Lob
	@Column(name = "mainPage", length = Integer.MAX_VALUE, nullable = true)
	private String mainPage;

	@Lob
	@Column(name = "extraOne", length = Integer.MAX_VALUE, nullable = true)
	private String extraOne;

	@Lob
	@Column(name = "extraTwo", length = Integer.MAX_VALUE, nullable = true)
	private String extraTwo;

	@Lob
	@Column(name = "extraThree", length = Integer.MAX_VALUE, nullable = true)
	private String extraThree;

	@Column(name = "Category", nullable = false)
	private String category;

	private boolean action;

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getUnitInStock() {
		return unitInStock;
	}

	public void setUnitInStock(int unitInStock) {
		this.unitInStock = unitInStock;
	}

	public String getMainPage() {
		return mainPage;
	}

	public void setMainPage(String mainPage) {
		this.mainPage = mainPage;
	}

	public String getExtraOne() {
		return extraOne;
	}

	public void setExtraOne(String extraOne) {
		this.extraOne = extraOne;
	}

	public String getExtraTwo() {
		return extraTwo;
	}

	public void setExtraTwo(String extraTwo) {
		this.extraTwo = extraTwo;
	}

	public String getExtraThree() {
		return extraThree;
	}

	public void setExtraThree(String extraThree) {
		this.extraThree = extraThree;
	}

	public boolean isAction() {
		return action;
	}

	public void setAction(boolean action) {
		this.action = action;
	}

}
