package com.diploma.Webshop;

import java.io.UnsupportedEncodingException;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.stereotype.Component;

import com.diploma.Webshop.Models.User;
import com.diploma.Webshop.Service.UserService;

@Component
public class BeforeAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

	@Autowired
	private UserService userService;

	@Autowired
	public void setAuthenticationManager(AuthenticationManager authManager) {
		super.setAuthenticationManager(authManager);
	}

	@Autowired
	@Override
	public void setAuthenticationFailureHandler(AuthenticationFailureHandler failureHandler) {
		super.setAuthenticationFailureHandler(failureHandler);
	}

	@Autowired
	@Override
	public void setAuthenticationSuccessHandler(AuthenticationSuccessHandler successHandler) {
		super.setAuthenticationSuccessHandler(successHandler);
	}

	public BeforeAuthenticationFilter() {
		setUsernameParameter("email");
		super.setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher("/login_form", "POST"));
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {
		String email = request.getParameter("email");
		User user = userService.findByEmail(email);

		if (user != null) {
			if (user.getRole().getName().contains("USER") && user.isTwoFacEnabled() == true
					&& user.isEnabled() == true) {

				if (user.isOTPRequired()) {
					return super.attemptAuthentication(request, response);
				}

				float spamScore = getGoogleRecaptchaScore();

				if (spamScore < 0.5) {
					try {
						userService.generateOneTimePassword(user);
						throw new InsufficientAuthenticationException("OTP");
					} catch (MessagingException | UnsupportedEncodingException ex) {
						throw new AuthenticationServiceException("Hiba az egyszeri kód küldésnél.");
					}
				}
			}
		}

		return super.attemptAuthentication(request, response);

	}

	private float getGoogleRecaptchaScore() {
		return 0.43f;
	}
}
