package com.diploma.Webshop.Controllers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.diploma.Webshop.Models.Cart;
import com.diploma.Webshop.Models.CartItem;
import com.diploma.Webshop.Models.Category;
import com.diploma.Webshop.Models.Contact;
import com.diploma.Webshop.Models.Product;
import com.diploma.Webshop.Models.Rating;
import com.diploma.Webshop.Models.Role;
import com.diploma.Webshop.Models.User;
import com.diploma.Webshop.Repositories.CartItemRepository;
import com.diploma.Webshop.Repositories.CartRepository;
import com.diploma.Webshop.Repositories.CategoryRepository;
import com.diploma.Webshop.Repositories.ContactRepository;
import com.diploma.Webshop.Repositories.ProductRepository;
import com.diploma.Webshop.Repositories.RatingRepository;
import com.diploma.Webshop.Repositories.UserRepository;
import com.diploma.Webshop.Service.ProductService;

@Controller
public class IndexController {
	@Autowired
	private UserRepository repo;

	@Autowired
	private ProductRepository productRepo;

	@Autowired
	private CategoryRepository categoryRepo;

	@Autowired
	private CartItemRepository cartItemRepo;
	
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private CartRepository cartRepo;

	@Autowired
	private ContactRepository contactRepo;

	@Autowired
	private ProductService productService;

	@Autowired
	private RatingRepository ratingRepo;

	@Autowired
	private UserRepository userRepo;

	@GetMapping("/")
	public String index(Model model) {
		return viewPage(model, 1);
	}

	@RequestMapping("/page/{pageNum}")
	public String viewPage(Model model, @PathVariable("pageNum") int pageNum) {

		Page<Product> page = productService.listAll(pageNum);

		List<Product> product = page.getContent();
		long totalItems = page.getNumberOfElements();
		int totalPages = page.getTotalPages();
		List<Category> category = categoryRepo.findAll();

		model.addAttribute("currentPage", pageNum);
		model.addAttribute("totalPages", totalPages);
		model.addAttribute("totalItems", totalItems);
		model.addAttribute("category", category);
		model.addAttribute("product", product);

		User user = new User();
		Role role = new Role();
		List<User> users = userRepo.findAll();

		if (users.isEmpty()==true) {
			user.setEmail("Admin@gmail.com");
			role.setName("MAIN_ADMIN");
			user.setRole(role);
			user.setFirstName("Admin");
			String password = "Admin123";
			user.setEnabled(true);
			user.setVerificationCode(null);
			user.setLastName(" ");
			String encodedPassword = passwordEncoder.encode(password);
			user.setPassword(encodedPassword);
			user.setPhoneNumber("06205528815");
			userRepo.save(user);

		}

		return "index";
	}

	@RequestMapping(value = "/index/{id}", method = RequestMethod.POST)
	public String packInTheCart(@PathVariable Long id, @RequestParam("db") Integer db) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = repo.findByEmail(auth.getName());
		if (user == null) {
			return "loginPLS";
		}
		if (db == null || db == 0) {
			return "redirect:/";
		}
		Product prod = productRepo.findByProductId(id);
		prod.setUnitInStock(prod.getUnitInStock() - db);
		productRepo.save(prod);

		Cart cart;
		if (user.getCart() == null) {
			cart = new Cart();

		} else {
			cart = user.getCart();
		}
		CartItem item = new CartItem();
		item.setQuantity(db);
		item.setCart(user.getCart());
		item.setTotalPriceDouble(prod.getPrice() * db);
		item.setProduct(prod);

		cart.setUser(user);

		List<CartItem> cartItems = new ArrayList<>();
		if (cart.getId() != null) {
			cartItems = cartItemRepo.findByCartId(cart.getId()).orElse(new ArrayList<>());
		}
		cart.setCartItems(cartItems);
		cartItems.add(item);
		cartItemRepo.save(item);
		int ar = 0;
		for (CartItem cartItem : cartItems) {
			ar += cartItem.getTotalPriceDouble();
			cart.setTotalPrice(ar);
		}
		user.setCart(cart);
		cartRepo.save(cart);
		repo.save(user);
		return "redirect:/";

	}

	@RequestMapping("/contact")
	public String contactUs(Model model) {
		Contact contact = new Contact();
		model.addAttribute("message", contact);
		return "contactUs";
	}

	@PostMapping("/process_contact")
	public String processContact(@ModelAttribute("contact") @Valid Contact contact, BindingResult bindingResult) {
		List<Contact> contacts = contactRepo.findAll();
		for (Contact contactok : contacts) {
			if (contactok.getEmail().toString().contains(contact.getEmail().toString())) {
				return "contact_error";
			}
		}

		if (bindingResult.hasErrors()) {
			return "contactUs";
		}

		contactRepo.save(contact);

		return "contact_succes";
	}

	@RequestMapping(value = "/index/view/{id}", method = RequestMethod.POST)
	public String viewProduct(@PathVariable Long id, Model model) {

		Product product = productRepo.findByProductId(id);
		Rating rating = new Rating();
		rating.setProduct(product);
		model.addAttribute("product", product);
		model.addAttribute("rating", rating);
		List<Rating> showRating = ratingRepo.findRatingByProduct(product);
		float sum = 0;
		int count = 0;
		for (Rating ratings : showRating) {
			sum += ratings.getStars();
			count++;
		}

		if (sum > 0) {
			sum = sum / count;
		}

		model.addAttribute("ratingShow", showRating);
		model.addAttribute("totalStars", sum);

		return "viewProduct";
	}

	@PostMapping("/process_rating")
	public String processRating(@Valid Product product, Rating rating) {
		rating.setProduct(product);

		rating.setCommentDate(new Date());

		ratingRepo.save(rating);
		return "redirect:/";
	}

	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public String searchInProductByName(@RequestParam("srch") String srch, Model model) {
		Product product = productRepo.findByName(srch);
		List<Category> categories = categoryRepo.findAll();
		if (product == null) {
			List<Product> products = productRepo.findAll();
			model.addAttribute("category", categories);
			model.addAttribute("product", products);

			return "index";
		}

		model.addAttribute("category", categories);
		model.addAttribute("product", product);

		return "index";
	}

	@RequestMapping(value = "/category/{id}", method = RequestMethod.POST)
	public ModelAndView categories(@PathVariable Long id) {
		Category category = categoryRepo.findByCategoryId(id);
		List<Category> categories = categoryRepo.findAll();
		List<Product> products = categoryRepo.findProductByCategoryNames(category.getCategoryName());
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("category", categories);
		modelAndView.addObject("product", products);
		modelAndView.setViewName("index");
		return modelAndView;
	}

}
