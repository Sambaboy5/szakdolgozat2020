package com.diploma.Webshop.Controllers;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.diploma.Webshop.Models.Cart;
import com.diploma.Webshop.Models.CartItem;
import com.diploma.Webshop.Models.Category;
import com.diploma.Webshop.Models.Contact;
import com.diploma.Webshop.Models.Invoice;
import com.diploma.Webshop.Models.OldContact;
import com.diploma.Webshop.Models.Order;
import com.diploma.Webshop.Models.PaidItems;
import com.diploma.Webshop.Models.Product;
import com.diploma.Webshop.Models.Rating;
import com.diploma.Webshop.Models.Role;
import com.diploma.Webshop.Models.User;
import com.diploma.Webshop.Repositories.CartItemRepository;
import com.diploma.Webshop.Repositories.CartRepository;
import com.diploma.Webshop.Repositories.CategoryRepository;
import com.diploma.Webshop.Repositories.ContactRepository;
import com.diploma.Webshop.Repositories.InvoiceRepository;
import com.diploma.Webshop.Repositories.OldContactRepository;
import com.diploma.Webshop.Repositories.OrderRepository;
import com.diploma.Webshop.Repositories.PaidItemsRepository;
import com.diploma.Webshop.Repositories.ProductRepository;
import com.diploma.Webshop.Repositories.RatingRepository;
import com.diploma.Webshop.Repositories.RoleRepository;
import com.diploma.Webshop.Repositories.UserRepository;
import com.diploma.Webshop.Service.MailService;

@Controller
public class AdminController {

	@Autowired
	private CategoryRepository categoryRepo;

	@Autowired
	private UserRepository repo;

	@Autowired
	private ContactRepository contactRepo;

	@Autowired
	private OldContactRepository oldContactRepo;

	@Autowired
	private ProductRepository productRepo;

	@Autowired
	private CartItemRepository cartItemRepo;

	@Autowired
	private CartRepository cartRepo;

	@Autowired
	private RoleRepository roleRepo;

	@Autowired
	private MailService mailService;

	@Autowired
	private OrderRepository orderRepo;

	@Autowired
	private InvoiceRepository invoiceRepo;

	@Autowired
	private RatingRepository ratingRepo;

	@Autowired
	private PaidItemsRepository paidItemsRepo;

	@RequestMapping(value = "/contact/delete/{id}", method = RequestMethod.GET)
	public String deleteContact(@PathVariable Long id) {
		Contact contact = contactRepo.findContactById(id);
		OldContact oldContact = new OldContact();
		oldContact.setEmail(contact.getEmail());
		oldContact.setMessage(contact.getMessage());
		oldContact.setName(contact.getName());
		oldContact.setReaded(contact.isReaded());
		oldContactRepo.save(oldContact);
		contactRepo.deleteContactById(id);
		return "redirect:/users";
	}

	@GetMapping("/users")
	public String listUsers(Model model) {
		List<User> listUsers = repo.findAll();
		List<Contact> contacts = contactRepo.findAll();
		List<OldContact> oldContacts = oldContactRepo.findAll();

		model.addAttribute("listUsers", listUsers);
		model.addAttribute("listContact", contacts);
		model.addAttribute("listOldContact", oldContacts);

		return "users";
	}

	@GetMapping("/boughtView")
	public String listBoughtView(Model model) {
		List<PaidItems> items = paidItemsRepo.findAll();

		model.addAttribute("items", items);
		

		return "boughtview";
	}

	@RequestMapping(value = "/users/delete/{id}", method = RequestMethod.GET)
	public String deleteUser(@PathVariable Long id) {
		User user = repo.findByUserId(id);
		List<Cart> cart = cartRepo.findAll();
		if (user.getCart() != null) {

			for (Cart carts : cart) {
				if (carts.getId() == user.getCart().getId()) {
					carts.setUser(null);
					cartRepo.save(carts);
				}
			}
			user.setCart(null);
		}
		List<Order> order = orderRepo.findAll();
		if (user.getOrder() != null) {
			for (Order orders : order) {
				if (orders.getOrderId() == user.getOrder().getOrderId()) {
					orders.setUser(null);
					orderRepo.save(orders);
				}
				if (orders.getUser() == null || orders.getUser().getId() == user.getId()) {
					orderRepo.deleteById(orders.getOrderId());
				}

			}
			user.setOrder(null);
		}
		List<Invoice> invoice = invoiceRepo.findAll();
		if (user.getInvoice() != null) {

			for (Invoice invoices : invoice) {
				if (invoices.getId() == user.getInvoice().getId()) {
					invoices.setUser(null);
					invoiceRepo.save(invoices);
				}
				if (invoices.getUser() == null || invoices.getUser().getId() == user.getId()) {
					invoiceRepo.deleteById(invoices.getId());
				}
			}
			user.setInvoice(null);
		}

		if (user.getInvoice() == null || user.getOrder() == null || user.getCart() == null) {
			for (Cart carts : cart) {
				if (carts.getUser() == null || carts.getUser().getId() == user.getId()) {
					carts.setUser(null);
					cartRepo.save(carts);
				}
			}
			for (Order orders : order) {

				if (orders.getUser() == null || orders.getUser().getId() == user.getId()) {
					orders.setUser(null);
					orderRepo.save(orders);
					orderRepo.deleteById(orders.getOrderId());
				}

			}
			for (Invoice invoices : invoice) {

				if (invoices.getUser() == null || invoices.getUser().getId() == user.getId()) {
					invoices.setUser(null);
					invoiceRepo.save(invoices);
					invoiceRepo.deleteById(invoices.getId());
				}
			}
		}
		List<PaidItems> paidItems = paidItemsRepo.findAll();

		for (PaidItems items : paidItems) {
			if (items.getUser().getId() == id) {

				paidItemsRepo.deleteById(items.getPaidItemsId());
			}

		}
		repo.save(user);
		repo.deleteUsersByFirstName(id);
		return "redirect:/users";
	}

	@GetMapping("/product")
	public String newPorducts(Model model) {
		List<Category> category = categoryRepo.findAll();
		Product product = new Product();

		model.addAttribute("category", category);
		model.addAttribute("product", product);
		return "newProducts";
	}

	@GetMapping("/newCategory")
	public String newCategory(Model model) {
		Category category = new Category();

		model.addAttribute("category", category);
		return "newCategory";
	}

	@PostMapping("/process_product")
	public String processProduct(@RequestParam("image") MultipartFile file,
			@RequestParam("imageOne") MultipartFile imageOne, @RequestParam("imageTwo") MultipartFile imageTwo,
			@RequestParam("imageThree") MultipartFile imageThree, Product product) throws IOException {

		String encodedString = Base64Utils.encodeToString(file.getBytes());
		String encodedString1 = Base64Utils.encodeToString(imageOne.getBytes());
		String encodedString2 = Base64Utils.encodeToString(imageTwo.getBytes());
		String encodedString3 = Base64Utils.encodeToString(imageThree.getBytes());
		product.setMainPage("data:image/jpeg;base64," + encodedString);
		product.setExtraOne("data:image/jpeg;base64," + encodedString1);
		product.setExtraTwo("data:image/jpeg;base64," + encodedString2);
		product.setExtraThree("data:image/jpeg;base64," + encodedString3);

		productRepo.save(product);

		return "product_succes";
	}

	@PostMapping("/process_category")
	public String processCategory(Category caregory) throws IOException {

		categoryRepo.save(caregory);

		return "redirect:/users";
	}

	@RequestMapping(value = "/users/contact/{id}", method = RequestMethod.GET)
	public String sendContactBack(@PathVariable Long id, Model model) {
		Contact contact = contactRepo.findContactById(id);
		model.addAttribute("contact", contact);
		return "contact_answer";
	}

	@RequestMapping(value = "/users/contact/ok", method = RequestMethod.POST)
	public String processAnswer(@ModelAttribute("contact") Contact contact, ModelMap modelMap) {
		try {
			String content = "Email: " + contact.getEmail() + "\n";
			content += "Tisztelt " + contact.getName() + ",\n\n\n\n";
			content += contact.getMessage() + "\n\n\n\nÜdvözlettel,\nGardrób turi";

			contact.setReaded(true);
			contactRepo.save(contact);
			mailService.send("Admin@gmail.com", contact.getEmail(), "Válasz", content);
		} catch (Exception e) {
			modelMap.put("msg", e.getMessage());
		}

		return "redirect:/users";
	}

	@RequestMapping(value = "/index/edit/{id}", method = RequestMethod.GET)
	public ModelAndView editProduct(@PathVariable Long id) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("id", id);

		modelAndView.setViewName("edit");
		return modelAndView;
	}

	@RequestMapping(value = "/users/update/{id}", method = RequestMethod.GET)
	public ModelAndView editUser(@PathVariable Long id) {
		ModelAndView modelAndView = new ModelAndView();
		List<Role> roles = roleRepo.findAll();
		modelAndView.addObject("id", id);
		modelAndView.addObject("roles", roles);
		modelAndView.setViewName("edit_user");
		return modelAndView;
	}

	@RequestMapping(value = "/index/edit/ok", method = RequestMethod.POST)
	public String editProductSuccess(@RequestParam("id") Integer id, @RequestParam("price") Integer price,
			@RequestParam("name") String name, @RequestParam("shortDescription") String shortDescription,
			@RequestParam("unitInStock") Integer unitInStock,
			@RequestParam(required = true, defaultValue = "false", name = "action") boolean action) {
		Product prod = productRepo.findByProductId(Integer.toUnsignedLong(id));
		prod.setUnitInStock(unitInStock);
		prod.setShortDescription(shortDescription);
		prod.setName(name);
		prod.setPrice(price);
		prod.setAction(action);

		productRepo.save(prod);
		return "redirect:/";
	}

	@RequestMapping(value = "/users/update/ok", method = RequestMethod.POST)
	public String editUsersSuccess(@RequestParam("id") Integer id, @RequestParam(value = "roles") String role) {

		List<Role> roles = roleRepo.findAll();
		User user = repo.findByUserId(Integer.toUnsignedLong(id));
		for (Role role2 : roles) {
			if (role2.getName().equals(role)) {

				user.setRole(role2);
			}
		}

		repo.save(user);
		return "redirect:/users";
	}

	@RequestMapping(value = "/index/delete/{id}", method = RequestMethod.GET)
	public String deleteProduct(@PathVariable Long id) {
		Product product = productRepo.findByProductId(id);
		List<CartItem> cartItems = cartItemRepo.findAll();
		for (CartItem cartItem : cartItems) {
			if (cartItem.getProduct().getId() == product.getId()) {
				cartItem.setProduct(null);

				cartItemRepo.save(cartItem);
				cartItemRepo.deleteCartItem(cartItem.getCartItemId());
			}

		}
		List<Rating> ratings = ratingRepo.findAll();
		for (Rating rating : ratings) {
			if (rating.getProduct().getId() == id) {
				ratingRepo.deleteRatingByProduct(rating.getCommentId());
			}
		}

		productRepo.deleteProductByFirstName(id);
		return "redirect:/";
	}
}
