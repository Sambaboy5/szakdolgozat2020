package com.diploma.Webshop.Controllers;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.diploma.Webshop.Models.BillingAddress;
import com.diploma.Webshop.Models.Cart;
import com.diploma.Webshop.Models.Role;
import com.diploma.Webshop.Models.ShippingAddress;
import com.diploma.Webshop.Models.User;
import com.diploma.Webshop.Repositories.RoleRepository;
import com.diploma.Webshop.Repositories.UserRepository;
import com.diploma.Webshop.Service.UserService;

@Controller
public class RegistrationController {

	@Autowired
	private UserRepository repo;

	@Autowired
	private UserService service;

	@Autowired
	private RoleRepository roleRepo;

	@RequestMapping("/registration_form")
	public String registration(Model model) {
		model.addAttribute("user", new User());
		model.addAttribute("shipAddress", new ShippingAddress());
		model.addAttribute("billAddress", new BillingAddress());
		return "registration";
	}

	@PostMapping("/process_register")
	public String processRegister(@ModelAttribute("user") @Valid User user, ShippingAddress shipping,
			BillingAddress billing, BindingResult bindingResult, HttpServletRequest request,
			@RequestParam(required = true, defaultValue = "false", name = "twoFactor") boolean twoFactor)
			throws UnsupportedEncodingException, MessagingException {
		List<User> user1 = repo.findAll();
		for (User users : user1) {
			if (users.getEmail().toString().contains(user.getEmail().toString())) {
				return "register_error";
			}
		}
		if (bindingResult.hasErrors()) {
			return "registration";
		}
		
		List<Role> role = roleRepo.findAll();
		for (Role roles : role) {
			if (roles.getName().contains("USER")) {
				user.setRole(roles);
				
			}
		}

		user.setTwoFacEnabled(twoFactor);
		user.setBillingAddress(billing);
		user.setShippingAddress(shipping);
		user.setCart(new Cart());


		service.register(user, getSiteURL(request));
		return "register_success";
	}

	private String getSiteURL(HttpServletRequest request) {
		String siteURL = request.getRequestURL().toString();
		return siteURL.replace(request.getServletPath(), "");
	}

	@GetMapping("/login_form")
	public String login(Model model) {
		User user = new User();
		model.addAttribute("user", user);
		return "login";

	}

	@GetMapping("/verify")
	public String verifyUser(@Param("code") String code) {
		if (service.verify(code)) {
			return "verify_succes";
		} else {
			return "verify_fail";
		}
	}

}
