package com.diploma.Webshop.Controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.diploma.Webshop.Models.Cart;
import com.diploma.Webshop.Models.CartItem;
import com.diploma.Webshop.Models.Order;
import com.diploma.Webshop.Models.Product;
import com.diploma.Webshop.Models.User;
import com.diploma.Webshop.Repositories.CartItemRepository;
import com.diploma.Webshop.Repositories.CartRepository;
import com.diploma.Webshop.Repositories.OrderRepository;
import com.diploma.Webshop.Repositories.ProductRepository;
import com.diploma.Webshop.Repositories.UserRepository;

@Controller
public class CartControlller {

	@Autowired
	private UserRepository repo;

	@Autowired
	private ProductRepository productRepo;

	@Autowired
	private CartItemRepository cartItemRepo;

	@Autowired
	private CartRepository cartRepo;

	@Autowired
	private OrderRepository orderRepo;

	@RequestMapping(value = "/cart", method = RequestMethod.GET)
	public ModelAndView shoppingCart() {
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = repo.findByEmail(auth.getName());
		if (user == null || user.getCart()==null) {
			modelAndView.setViewName("shoppingCart");
			return modelAndView;
		}
		List<CartItem> items = user.getCart().getCartItems();
		List<Product> products = new ArrayList<>();
		for (CartItem cartItem : items) {
			products.add(cartItem.getProduct());
		}

		List<Cart> cart = cartRepo.findAll();
		if (cart==null) {
			modelAndView.setViewName("redirect:/index");
			return modelAndView;
		}
		modelAndView.addObject("items", items);
		modelAndView.addObject("cart", cart);
		modelAndView.setViewName("shoppingCart");
		return modelAndView;
	}

	@RequestMapping(value = "/cart/delete/{id}", method = RequestMethod.GET)
	public String deleteFromCart(@PathVariable Long id) {
		cartItemRepo.deleteCartItem(id);
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = repo.findByEmail(auth.getName());
		Cart cart = cartRepo.findByCartId(user.getCart().getId());
		List<CartItem> cartItems = cartItemRepo.findAll();
		int ar = 0;
		for (CartItem cartItem : cartItems) {
			if (cartItem.getCart().getId() == user.getCart().getId()) {
				ar += cartItem.getTotalPriceDouble();
				cart.setTotalPrice(ar);
			}
			if (ar == 0) {
				cart.setTotalPrice('0');

			}
		}
		user.setCart(cart);
		cartRepo.save(cart);
		repo.save(user);

		return "redirect:/";
	}

	@RequestMapping(value = "/cart/update/{id}", method = RequestMethod.GET)
	public String updateFromCart(@PathVariable Long id, @RequestParam("db") Integer db) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = repo.findByEmail(auth.getName());
		CartItem cartItems = cartItemRepo.findByCartItemId(id);
		Product prod = productRepo.findByProductId(cartItems.getProduct().getId());
		Cart cart = cartRepo.findByCartId(user.getCart().getId());
		cartItems.setQuantity(db);
		cartItems.setTotalPriceDouble(prod.getPrice() * db);
		List<CartItem> item = cartItemRepo.findAll();
		int ar = 0;
		for (CartItem cartItem : item) {
			if (cartItem.getCart().getId() == user.getCart().getId() && cartItem.getCart().getId()!=null) {
				ar += cartItem.getTotalPriceDouble();
				cart.setTotalPrice(ar);
			}
		}
		cartItemRepo.save(cartItems);
		user.setCart(cart);
		cartRepo.save(cart);
		repo.save(user);
		return "redirect:/";
	}

	@RequestMapping("/order/{id}")
	public String createOrder(@PathVariable Long id, Model model) {
		Cart cart = cartRepo.findByCartId(id);
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = repo.findByEmail(auth.getName());
		if (user == null) {
			return "loginPLS";
		}
		Order order;
		user = cart.getUser();
		if (user.getOrder() == null) {
			order = new Order();
			order.setCart(cart);
		} else {
			order = user.getOrder();
		}
		user.setOrder(order);
		if (user.getOrder().getCart().getId() == cart.getId()) {
			order.setUser(user);

			order.setShippingAddress(user.getShippingAddress());

		} else {
			user.setOrder(null);
			repo.save(user);
			orderRepo.save(order);
			return "shopping_count";
		}

		repo.save(user);
		orderRepo.save(order);
		List<CartItem> items = user.getCart().getCartItems();
		model.addAttribute("items", items);

		return "shopping_count";
	}
}
