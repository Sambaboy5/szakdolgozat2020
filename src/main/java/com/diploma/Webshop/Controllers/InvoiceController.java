package com.diploma.Webshop.Controllers;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.lowagie.text.DocumentException;

import com.diploma.Webshop.Models.Cart;
import com.diploma.Webshop.Models.CartItem;
import com.diploma.Webshop.Models.Invoice;
import com.diploma.Webshop.Models.Order;
import com.diploma.Webshop.Models.PaidItems;
import com.diploma.Webshop.Models.User;
import com.diploma.Webshop.Models.UserPDFExporter;
import com.diploma.Webshop.Repositories.CartItemRepository;
import com.diploma.Webshop.Repositories.CartRepository;
import com.diploma.Webshop.Repositories.InvoiceRepository;
import com.diploma.Webshop.Repositories.OrderRepository;
import com.diploma.Webshop.Repositories.PaidItemsRepository;
import com.diploma.Webshop.Repositories.UserRepository;
import com.diploma.Webshop.Service.MailService;

@Controller
public class InvoiceController {

	@Autowired
	private CartRepository cartRepo;
	
	@Autowired
	private CartItemRepository cartItemRepo;

	@Autowired
	private OrderRepository orderRepo;

	@Autowired
	private InvoiceRepository invoiceRepo;

	@Autowired
	private UserRepository repo;

	@Autowired
	private MailService mailService;
	
	@Autowired
	private PaidItemsRepository paidItemsRepo;

	@GetMapping("/users/export/pdf/{id}")
	public String exportToPDF(@PathVariable Long id, HttpServletResponse response)
			throws DocumentException, IOException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = repo.findByEmail(auth.getName());
		Cart cart = cartRepo.findByCartId(id);
		user = cart.getUser();

		DateFormat dateFormatter = new SimpleDateFormat("yyyy_MM_dd");
		String headerValue = "invoice_" + dateFormatter.format(new Date()) + ".pdf";

		Order order = orderRepo.findByOrderId(user.getOrder().getOrderId());
		List<Order> orders = new ArrayList<Order>();
		orders.add(order);

		UserPDFExporter exporter = new UserPDFExporter(orders);
		Invoice invoice;
		if (user.getInvoice() == null) {
			invoice = new Invoice();
		} else {
			invoice = user.getInvoice();
		}
		invoice.setUser(user);

		byte[] base64Pdf = exporter.export(response);
		invoice.setInvoiceName(headerValue);
		invoice.setData(base64Pdf);
		user.setInvoice(invoice);
		invoiceRepo.save(invoice);
		repo.save(user);

		try {
			String content = "Email: " + order.getUser().getEmail() + "\n";
			content += "Tisztelt " + order.getUser().getFirstName() + " " + order.getUser().getLastName() + ",\n\n\n\n";
			content += "Mellékelve küldjük a számlát." + "\n\n\n\nÜdvözlettel,\nGardrób turi";

			System.out.print(invoice.getData().toString());
			mailService.sendOrder("Admin@gmail.com", order.getUser().getEmail(), "Számla", content, invoice.getData());
		} catch (Exception e) {
			e.getMessage();
		}
		List<CartItem> cartItems = cartItemRepo.findAll();
		for (CartItem cartItem : cartItems) {
			if (cartItem.getCart().getId()==id) {
				PaidItems items = new PaidItems();
				items.setCart(cartItem.getCart());
				items.setProduct(cartItem.getProduct());
				items.setQuantity(cartItem.getQuantity());
				items.setTotalPriceDouble(cartItem.getTotalPriceDouble());
				items.setUser(cartItem.getCart().getUser());
				paidItemsRepo.save(items);
				cartItemRepo.deleteCartItem(cartItem.getCartItemId());
			}
		}
		user.setInvoice(null);
		user.setOrder(null);
		repo.save(user);
		return "shipping_succes";

	}
}
