package com.diploma.Webshop.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.diploma.Webshop.Models.BillingAddress;
import com.diploma.Webshop.Models.ShippingAddress;
import com.diploma.Webshop.Models.User;
import com.diploma.Webshop.Repositories.BillingRepository;
import com.diploma.Webshop.Repositories.ShippingRepository;
import com.diploma.Webshop.Repositories.UserRepository;

@Controller
public class UserController {

	@Autowired
	private ShippingRepository shipRepo;

	@Autowired
	private BillingRepository billRepo;

	@Autowired
	private UserRepository repo;

	@GetMapping("/myAccount")
	public String account(Model model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = repo.findByEmail(auth.getName());
		model.addAttribute("account", user);

		return "account";
	}

	@RequestMapping(value = "/myAccount/update/shipping/{id}", method = RequestMethod.GET)
	public ModelAndView updateShipAccount(@PathVariable Long id) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("id", id);
		ShippingAddress shippingAddress = shipRepo.findByShippingId(id);
		modelAndView.setViewName("edit_shipping");

		return modelAndView;
	}

	@RequestMapping(value = "/myAccount/update/twoFac", method = RequestMethod.GET)
	public String updateTwoFac(@RequestParam(required = true, defaultValue = "false", name = "switch") boolean twoFac) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = repo.findByEmail(auth.getName());
		user.setTwoFacEnabled(twoFac);

		repo.save(user);
		return "redirect:/";
	}

	@RequestMapping(value = "/myAccount/update/billing/{id}", method = RequestMethod.GET)
	public ModelAndView updateBillAccount(@PathVariable Long id) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("id", id);
		BillingAddress billingAddress = billRepo.findByBillingAddress(id);
		modelAndView.setViewName("edit_billing");

		return modelAndView;
	}

	@RequestMapping(value = "/myAccount/update/billing/ok", method = RequestMethod.POST)
	public String updateBillingAccountSucces(@RequestParam("id") Integer id, @RequestParam("address") String address,
			@RequestParam("city") String city, @RequestParam("state") String state,
			@RequestParam("country") String country, @RequestParam("zipcode") Integer zipcode) {
		BillingAddress billingAddress = billRepo.findByBillingAddress(Integer.toUnsignedLong(id));
		billingAddress.setAddress(address);
		billingAddress.setCity(city);
		billingAddress.setCountry(country);
		billingAddress.setCounty(state);

		billRepo.save(billingAddress);

		return "redirect:/";
	}

	@RequestMapping(value = "/myAccount/update/shipping/ok", method = RequestMethod.POST)
	public String Shipping(@RequestParam("id") Integer id, @RequestParam("address") String address,
			@RequestParam("city") String city, @RequestParam("state") String state,
			@RequestParam("country") String country, @RequestParam("zipcode") Integer zipcode) {
		ShippingAddress shippingAddress = shipRepo.findByShippingId(Integer.toUnsignedLong(id));
		shippingAddress.setAddress(address);
		shippingAddress.setCity(city);
		shippingAddress.setCountry(country);
		shippingAddress.setCounty(state);

		shipRepo.save(shippingAddress);

		return "redirect:/";
	}
}
