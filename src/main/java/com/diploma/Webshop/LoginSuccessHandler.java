package com.diploma.Webshop;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.diploma.Webshop.Models.User;
import com.diploma.Webshop.Repositories.UserRepository;
import com.diploma.Webshop.Service.UserService;

@Component
public class LoginSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

	@Autowired
	private UserService userService;

	@Autowired
	private UserRepository userRepo;

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {

		CustomUserDetails customerDetails = (CustomUserDetails) authentication.getPrincipal();

		User user = userRepo.findByEmail(customerDetails.getUsername());

		if (user.isOTPRequired()) {
			userService.clearOTP(user);
		}

		super.onAuthenticationSuccess(request, response, authentication);
	}

}