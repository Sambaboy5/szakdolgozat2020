package com.diploma.Webshop.Service;

public interface MailService {

	public void send(String fromAddress, String toAddress, String subject, String content) throws Exception;

	public void sendOrder(String fromAddress, String email, String subject, String content, byte[] data)
			throws Exception;

}
