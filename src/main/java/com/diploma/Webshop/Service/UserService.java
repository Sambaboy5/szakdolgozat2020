package com.diploma.Webshop.Service;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.security.auth.login.AccountNotFoundException;
import net.bytebuddy.utility.RandomString;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Sort;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import com.diploma.Webshop.Models.User;
import com.diploma.Webshop.Repositories.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepo;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private JavaMailSender mailSender;

	public User findByEmail(String email) {
		return userRepo.findByEmail(email);
	}

	public List<User> listAll() {
		return userRepo.findAll(Sort.by("email").ascending());
	}

	public void register(User user, String siteURL) throws UnsupportedEncodingException, MessagingException {
		String encodedPassword = passwordEncoder.encode(user.getPassword());
		user.setPassword(encodedPassword);
		String randomCode = RandomString.make(64);
		user.setVerificationCode(randomCode);
		user.setEnabled(false);

		userRepo.save(user);
		sendVerificationEmail(user, siteURL);
	}

	private void sendVerificationEmail(User user, String siteURL)
			throws MessagingException, UnsupportedEncodingException {
		String toAddress = user.getEmail();
		String fromAddress = "Admin@gmail.com";
		String senderName = "Gardrób turi";
		String subject = "Email hitelesítés!";
		String content = "Kedves [[name]],<br>" + "Email hitelesítéshez nyomja meg az alábbi linket:<br>"
				+ "<h3><a href=\"[[URL]]\" target=\"_self\">Hitelesítés</a></h3>" + "Köszönettel,<br>"
				+ "Gardrób turi.";

		MimeMessage message = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message);

		helper.setFrom(fromAddress, senderName);
		helper.setTo(toAddress);
		helper.setSubject(subject);

		content = content.replace("[[name]]", user.getFirstName());
		String verifyURL = siteURL + "/verify?code=" + user.getVerificationCode();

		content = content.replace("[[URL]]", verifyURL);

		helper.setText(content, true);

		mailSender.send(message);
	}

	public boolean verify(String verificationCode) {
		User user = userRepo.findByVerificationCode(verificationCode);

		if (user == null || user.isEnabled()) {
			return false;
		} else {
			user.setVerificationCode(null);
			user.setEnabled(true);
			userRepo.save(user);

			return true;
		}

	}

	public void updateResetPasswordToken(String token, String email) throws AccountNotFoundException {
		User user = userRepo.findByEmail(email);
		if (user != null) {
			user.setResetPasswordToken(token);
			userRepo.save(user);
		} else {
			throw new AccountNotFoundException("Nincs ilyen email című felhasználó: " + email);
		}
	}

	public User getByResetPasswordToken(String token) {
		return userRepo.findByResetPasswordToken(token);
	}

	public void updatePassword(User user, String newPassword) {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String encodedPassword = passwordEncoder.encode(newPassword);
		user.setPassword(encodedPassword);

		user.setResetPasswordToken(null);
		userRepo.save(user);
	}

	public void sendForgotEmail(String recipientEmail, String link)
			throws MessagingException, UnsupportedEncodingException {
		MimeMessage message = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message);

		helper.setFrom("Admin@gmail.com", "Gardrób turi");
		helper.setTo(recipientEmail);

		String subject = "Jelszó csere";

		String content = "<p>Üdvözlöm,</p>" + "<p>Ön kérvényezte a jelszó cserét.</p>"
				+ "<p>Kattintson a linkre jelszava megváltoztatásához</p>" + "<p><a href=\"" + link
				+ "\">Jelszó megváltoztatása</a></p>" + "<br>"
				+ "<p>Ha mégis eszébe jutott a jelszava, akkor ne vegye figyelembe ezt az üzenetet, "
				+ "vagy ha nem ön kérvényezte.</p>" + "<br>" + "<p>Üdvözlettel, " + "<br>" + "Gardrób turi.</p>";

		helper.setSubject(subject);

		helper.setText(content, true);

		mailSender.send(message);
	}

	public void generateOneTimePassword(User user) throws UnsupportedEncodingException, MessagingException {
		String OTP = RandomString.make(8);
		String encodedOTP = passwordEncoder.encode(OTP);

		user.setOneTimePassword(encodedOTP);
		user.setOtpRequestedTime(new Date());

		userRepo.save(user);

		sendOTPEmail(user, OTP);

	}

	public void sendOTPEmail(User user, String OTP) throws UnsupportedEncodingException, MessagingException {
		MimeMessage message = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message);

		helper.setFrom("Admin@gmail.com", "Gardród turi jelszó");
		helper.setTo(user.getEmail());

		String subject = "Egyszeri belépési jelszó";

		String content = "<p>Kedves " + user.getFirstName() + "</p>"
				+ "<p>Biztonsági okokból önnek egyszeri jelszót kell beírnia! " + "Az ön egyszeri jelszava: </p>"
				+ "<p><b>" + OTP + "</b></p>" + "<br>" + "<p>Az egyszeri jelszó 5 percig érvényes.</p>" + "<br>"
				+ "<p>Üdvözlettel, " + "<br>" + "Gardrób turi.</p>";
		helper.setSubject(subject);

		helper.setText(content, true);

		mailSender.send(message);

	}

	public void clearOTP(User user) {
		user.setOneTimePassword(null);
		user.setOtpRequestedTime(null);
		userRepo.save(user);
	}
}
