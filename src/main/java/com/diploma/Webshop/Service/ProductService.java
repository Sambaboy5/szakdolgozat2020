package com.diploma.Webshop.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.diploma.Webshop.Models.Product;
import com.diploma.Webshop.Repositories.PagingProductRepository;

@Service
public class ProductService {
	@Autowired
	private PagingProductRepository pageRepo;

	public Page<Product> listAll(int pageNum) {
		int pageSize = 12;

		Pageable pageable = PageRequest.of(pageNum - 1, pageSize);

		return pageRepo.findAll(pageable);
	}

	public Page<Product> listAllno() {
		int pageSize = 24;

		Pageable pageable = PageRequest.of(1, pageSize);

		return pageRepo.findAll(pageable);
	}

}
