package com.diploma.Webshop;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import com.diploma.Webshop.Models.User;
import com.diploma.Webshop.Service.UserService;

@Component
public class LoginFailureHandler extends SimpleUrlAuthenticationFailureHandler {

	@Autowired
	private UserService userService;

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws IOException, ServletException {

		String redirectURL;
		String email = request.getParameter("email");

		request.setAttribute("email", email);

		redirectURL = "/login_form?error&email=" + email;

		if (exception.getMessage().contains("OTP")) {
			redirectURL = "/login_form?otp=true&email=" + email;
		} else {
			User user = userService.findByEmail(email);
			if (user != null) {

				if (user.isOTPRequired()) {
					redirectURL = "/login_form?otp=true&email=" + email;
				}
			}
		}

		super.setDefaultFailureUrl(redirectURL);
		super.onAuthenticationFailure(request, response, exception);
	}

}