package com.diploma.Webshop;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.annotation.Rollback;

import com.diploma.Webshop.Models.User;
import com.diploma.Webshop.Repositories.UserRepository;
 
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@Rollback(false)
public class UserRepositoryTests {
	 @Autowired
	    private UserRepository repo;
	 

	    @Autowired
	    private TestEntityManager entityManager;
	    
	    @Test
	    public void testCreateUser() {
	        User user = new User();
	        user.setEmail("Admin@gmail.com");
	        user.setPassword("password");
	        user.setFirstName("Admin");
	        user.setLastName("Admin");
	         
	        User savedUser = repo.save(user);
	         
	        User existUser = entityManager.find(User.class, savedUser.getId());
	         
	        assertThat(user.getEmail()).isEqualTo(existUser.getEmail());
	         
	    }
}
